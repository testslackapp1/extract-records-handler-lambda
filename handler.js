const AWS = require('aws-sdk');

const dynamoDb = new AWS.DynamoDB.DocumentClient({
  api_version: '2012-08-10',
  region: 'us-west-1'
});

exports.handler = async (request) => {
  const params = {
    TableName: 'testSlackAppRecords',
  };

  let records = [];

  try {
    records = await dynamoDb.scan(params).promise();
    console.log(JSON.stringify(records));
    return {
      statusCode: 200,
      body: JSON.stringify(records.Items)
    };
  } catch (e) {
    return { statusCode: 400 };
  }
};
